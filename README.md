User Story.
------------------------
The system provides a simple GUI page that the user can enter a SoundCloud user id in.
After posting the id to the system, the system displays a page with the following:
All the songs uploaded by the user id. If the song is downloadable, there is a "Copy to S3" button/link.

Upon clicking the copy to s3 button, the system downloads the track and uploads to s3.
Theuploaded file should have a unique name (so it doesn't override other files).

Application setUp
---------------------
- From the root(Ubit) folder run command :
``mvn clean install``
- Navigate into the ubit-web folder and run the command :
``mvn package tomcat7:run``
- Access the app via the browser at
``http://localhost:8080/ubitsound``

Application Usage
--------------------------------
- Configure SoundCloud or Amazon WS credentials by clicking on "Configure Souncloud" or "Configure AWS" respectively

- For Configure Souncloud put in mind the following mapping.
        Secret Key is Your Soundcloud Secret Key and Client Id is Your Soundcloud Client Id

- For Configure AWS put in mind the following mapping.
        AWS S3 SecretKey is Your AWS Secret Key
        AWS S3 Access Key is Your AWS access Key
        Bucket is AWS bucket name
- To Transfer file , go to the menu page and click on the Transfer file.
 username is the sound cloud username and password is the soundcloud password. To send a file to
 AWS S3 click on the link for a file and follow the page to send to s3 . Once the transfer is done you would
 be directed to the menu.