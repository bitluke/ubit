<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<html>
<head>
    <title>Tracks</title>
</head>
<body>
<s:url namespace="/" action="MenuPage" var="urlTagSC"/>
<s:a href="%{urlTagSC}">To Menu</s:a><br/>
<display:table id="track" cellspacing="20" style="align:center;" requestURI="/ubit/SoundListPage" pagesize="10" name="allTracks">
    <display:column  title="ID"> <c:out value="${track.id}"/></display:column>
    <display:column  title="Title"> <c:out value="${track.title}"/></display:column>
    <display:column  title="AWS S3">
        <c:choose>
            <c:when test="${track.downloadable}">
                <s:url action="AWSLoginPage" var="urlTag" >
                    <s:param name="trackId">${track.id}</s:param>
                </s:url>
                <s:a href="%{urlTag}">Send</s:a>
            </c:when>
            <c:otherwise>
                No Available
            </c:otherwise>
        </c:choose>
    </display:column>
</display:table>
</body>
</html>