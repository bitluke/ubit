<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Menu</title>
</head>
<body>
<h3>SoundCloud to AWS uploader</h3>
<s:url action="ConfigureSC" var="urlTagSC"/>
[<s:a href="%{urlTagSC}">Configure Souncloud</s:a>]
<s:url action="ConfigureAWS" var="urlTagAWS"/>
[<s:a href="%{urlTagAWS}">Configure AWS</s:a>]
<s:url action="LoginPage" var="urlTag"/>
[<s:a href="%{urlTag}">Transfer Files</s:a>]
</body>
</html>
