<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
<head>
    <title>Sign On</title>
</head>

<body>

<h3>To AWS</h3>
<s:url namespace="/" action="MenuPage" var="urlTagSC"/>
<s:a href="%{urlTagSC}">To Menu</s:a>
<s:form action="SendToS3">
    <s:label label="Id" name="track.id"/>
    <s:label label="Title" name="track.title"/>
    <s:hidden name="trackId"/>
    <s:label disabled="true" label="Bucket Name" name="bucketName"/>
    <s:submit value="Send" />
</s:form>
</body>
</html>
