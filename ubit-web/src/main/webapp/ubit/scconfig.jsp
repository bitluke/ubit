<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<s:url namespace="/" action="MenuPage" var="urlTagSC"/>
<s:a href="%{urlTagSC}">To Menu</s:a>
<h3>Enter Sound Cloud Credentials</h3>
<s:form action="SaveConfigureSC">
    <s:textfield label="Client Id" name="scclientid"/>
    <s:textfield label="Secret Key" name="scclientsecret"/>
    <s:submit/>
</s:form>
</div>
</div>
</body>
</html>
