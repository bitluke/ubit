<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html lang="en">
<head>
    <title>Ubit|Login</title>
</head>
<body>
<h3>Enter Sound Cloud Credentials</h3>
<s:url namespace="/" action="MenuPage" var="urlTagSC"/>
<s:a href="%{urlTagSC}">To Menu</s:a>
<s:actionerror/>
<s:actionmessage/>
<s:form  action="ubit/UbitLogin">
    <s:textfield label="Username" name="username"/>
    <s:password label="Password" name="password"/>
    <s:submit/>
</s:form>
</div>
</body>
</html>
