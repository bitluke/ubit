<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h3>Enter Amazon Credentials</h3>
<s:url namespace="/" action="MenuPage" var="urlTagSC"/>
<s:a href="%{urlTagSC}">To Menu</s:a>
<s:form action="SaveConfigureAWS">
    <s:password label="AWS S3 SecretKey" name="awssecretkey"/>
    <s:textfield label="AWS S3 AccessKey" name="awsacesskey"/>
    <s:textfield label="Bucket Name" name="bucket"/>
    <s:submit/>
</s:form>
</div>
</div>
</body>
</html>