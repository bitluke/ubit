package com.bfs.ubit.web;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;

public class ConfigAction extends BaseAction {
    Logger logger = Logger.getLogger(getClass().getName());
    private String scclientid;
    private String scclientsecret;
    private String awssecretkey;
    private String awsacesskey;
    private String bucket;
    private Util util = new Util();


    public String writeAWS() {
        Map<String, String> awsCredentials = new HashMap<>();
        awsCredentials.put(Constants.AWS_SECRET_KEY, awssecretkey);
        awsCredentials.put(Constants.AWS_ACCESSKEY, awsacesskey);
        awsCredentials.put(Constants.BUCKET_NAME, bucket);
        storeCredentials(awsCredentials);
        return SUCCESS;
    }

    public String getScclientid() {
        return scclientid;
    }

    public void setScclientid(String scclientid) {
        this.scclientid = scclientid;
    }

    public String getScclientsecret() {
        return scclientsecret;
    }

    public void setScclientsecret(String scclientsecret) {
        this.scclientsecret = scclientsecret;
    }

    public String getAwssecretkey() {
        return awssecretkey;
    }

    public void setAwssecretkey(String awssecretkey) {
        this.awssecretkey = awssecretkey;
    }

    public String getAwsacesskey() {
        return awsacesskey;
    }

    public void setAwsacesskey(String awsacesskey) {
        this.awsacesskey = awsacesskey;
    }

    public String writeSC() {
        Map<String, String> scCredentials = new HashMap<>();
        scCredentials.put(Constants.SOUNDCLOUD_PASS, scclientsecret);
        scCredentials.put(Constants.SOUNDCLOUD_USERNAME, scclientid);
        storeCredentials(scCredentials);
        return SUCCESS;
    }



    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    private void storeCredentials(Map<String, String> credentials) {
        Properties prop = util.readPropertyFile();
        OutputStream output = null;

        try {

            output = new FileOutputStream(getClass().getClassLoader().getResource("config.properties").getFile());

            for (String entry : credentials.keySet()) {
                prop.setProperty(entry, credentials.get(entry));
            }

            // save properties to project root folder
            prop.store(output, null);

        } catch (IOException io) {
            logger.severe(io.getMessage());
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    logger.severe(e.getMessage());
                }
            }

        }
    }

}
