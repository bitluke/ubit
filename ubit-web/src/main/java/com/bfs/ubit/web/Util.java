package com.bfs.ubit.web;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;


public class Util {
    Logger logger = Logger.getLogger(getClass().getName());
    public Properties readPropertyFile() {
        Properties prop = new Properties();
        try {
            prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));

        } catch (IOException io) {
            logger.severe(io.getMessage());
        }
        return prop;
    }
}
