package com.bfs.ubit.web;

import com.bfs.ubit.library.SoundCloud;
import com.bfs.ubit.library.Track;

import java.util.ArrayList;
import java.util.logging.Logger;

public class SoundAction extends BaseAction {
    Logger logger = Logger.getLogger(getClass().getName());
    private Track track;
    private ArrayList<Track> allTracks;

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public ArrayList<Track> getAllTracks() {
        return allTracks;
    }

    public void setAllTracks(ArrayList<Track> allTracks) {
        this.allTracks = allTracks;
    }

    public String menu() throws Exception {
        allTracks = ((SoundCloud) getSession().get("soundcloud")).getMeTracks();
        logger.fine("Menu built");
        return SUCCESS;
    }
}
