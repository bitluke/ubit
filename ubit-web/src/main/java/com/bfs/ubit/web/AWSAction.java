package com.bfs.ubit.web;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.bfs.ubit.library.SoundCloud;
import com.bfs.ubit.library.Track;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import java.util.logging.Logger;


public class AWSAction extends BaseAction {
    Logger logger = Logger.getLogger(getClass().getName());
    AmazonS3 s3Client;
    private Long trackId;
    private Track track;
    private String username;
    private String susername;
    private String password;
    private String spassword;
    private String bucketName;

    private Util util = new Util();


    public String send() throws Exception {
        clearActionErrors();
        Properties p = util.readPropertyFile();

        bucketName = p.getProperty(Constants.BUCKET_NAME);
        susername = p.getProperty(Constants.AWS_ACCESSKEY);
        spassword = p.getProperty(Constants.AWS_SECRET_KEY);

        if ((bucketName != null && !bucketName.isEmpty())
                & (susername != null && !susername.isEmpty())
                & (spassword != null && !spassword.isEmpty())) {

            String amazonFileUploadLocationOriginal = bucketName + "/";

            s3Client = new AmazonS3Client(new BasicAWSCredentials(susername, spassword));

            if (!s3Client.doesBucketExist(bucketName)) {
                s3Client.createBucket(bucketName);
            }

            ObjectMetadata objectMetadata = new ObjectMetadata();
            track = ((SoundCloud) getSession().get(Constants.SOUNDCLOUD)).getTrack(trackId.intValue());

            FileInputStream fis = downloadFile(track.getDownloadUrl(), String.valueOf(track.getId()));

            PutObjectResult result = s3Client.putObject(new PutObjectRequest(amazonFileUploadLocationOriginal,
                    track.getTitle() + "-" + track.getId(), fis, objectMetadata));

            File file = new File(track.getTitle() + "-" + track.getId());

            logger.fine("ETag".concat(result.getETag()));
            logger.fine("file deleted" + file.delete());
            return SUCCESS;
        }

        addActionError("Use the Menu to update AWS parameters");
        return INPUT;
    }


    private FileInputStream downloadFile(String url, String trackid) throws IOException {
        //https://api.soundcloud.com/tracks/146832631/download?client_id=9d1d0db5303804474302b4f9da10e02c&oauth_token=1-16343-76999283-99b92ec068f2ec1
        String urlString = url + "?client_id=" + ((SoundCloud) getSession().get(Constants.SOUNDCLOUD)).getApp_client_id()
                + "&oauth_token=" + ((SoundCloud) getSession().get(Constants.SOUNDCLOUD)).getToken().access;
        if (isRedirected(urlString)) {
            URL obj = new URL(urlString);
            HttpURLConnection conn = setUpConnection(obj);
            urlString = conn.getHeaderField("Location");
        }
        File file = new File(track.getTitle() + "-" + trackid);
        org.apache.commons.io.FileUtils.copyURLToFile(new URL(urlString), file);
        return new FileInputStream(file);
    }


    private boolean isRedirected(String url) {
        try {
            URL obj = new URL(url);
            HttpURLConnection conn = setUpConnection(obj);
            int status = conn.getResponseCode();
            if (status != HttpURLConnection.HTTP_OK) {
                if (status == HttpURLConnection.HTTP_MOVED_TEMP
                        || status == HttpURLConnection.HTTP_MOVED_PERM
                        || status == HttpURLConnection.HTTP_SEE_OTHER)
                    return true;
            }
        } catch (IOException e) {
           logger.severe(e.getMessage());
        }

        return false;
    }

    private HttpURLConnection setUpConnection(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(5000);
        conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
        conn.addRequestProperty("User-Agent", "Mozilla");
        conn.addRequestProperty("Referer", "google.com");
        return conn;
    }

    public String retreiveTrack() throws Exception {
        track = ((SoundCloud) getSession().get(Constants.SOUNDCLOUD)).getTrack(trackId.intValue());
        return SUCCESS;
    }


    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public Long getTrackId() {
        return trackId;
    }

    public void setTrackId(Long trackId) {
        this.trackId = trackId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSusername() {
        Properties p = util.readPropertyFile();
        susername = p.getProperty(Constants.AWS_ACCESSKEY);
        return susername;
    }

    public void setSusername(String susername) {
        this.susername = susername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSpassword() {
        Properties p = util.readPropertyFile();
        spassword = p.getProperty(Constants.AWS_SECRET_KEY);
        return spassword;
    }

    public void setSpassword(String spassword) {
        this.spassword = spassword;
    }

    public String getBucketName() {
        Properties p = util.readPropertyFile();
        bucketName = p.getProperty(Constants.BUCKET_NAME);
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }


}
