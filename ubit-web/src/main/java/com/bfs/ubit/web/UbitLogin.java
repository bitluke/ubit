package com.bfs.ubit.web;

import com.bfs.ubit.library.SoundCloud;
import com.bfs.ubit.library.User;
import com.opensymphony.xwork2.inject.Inject;

import java.util.Properties;
import java.util.logging.Logger;


public class UbitLogin extends BaseAction {
    Logger logger = Logger.getLogger(getClass().getName());
    private boolean loggedIn;
    private User user;
    private SoundCloud soundcloud;
    private String password;
    private String username;
    private String scclientid;
    private String scclientsecret;
    private Util util = new Util();
    public UbitLogin() {

    }

    public String login() throws Exception {
        clearActionErrors();
        Properties p = util.readPropertyFile();

        scclientid = p.getProperty(Constants.SOUNDCLOUD_USERNAME);
        scclientsecret = p.getProperty(Constants.SOUNDCLOUD_PASS);

        if((scclientid != null && !scclientid.isEmpty())
                & (username != null && !username.isEmpty())
                & (password != null && !password.isEmpty())
                & (scclientsecret != null && !scclientsecret.isEmpty())

                ){

            soundcloud = new SoundCloud(scclientid, scclientsecret, username, password);
            user = soundcloud.getMe();
            getSession().put("scid", scclientid);
            getSession().put("scsecret", scclientsecret);
            getSession().put(Constants.SOUNDCLOUD, soundcloud);
            getSession().put("usr", user);
            loggedIn = true;
            getSession().put("logedin", loggedIn);
            return SUCCESS;
        }else {
            addActionError("Update Sound cloud parameter with the Menu then Specify your username and password");
            return INPUT;
        }

    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SoundCloud getSoundcloud() {
        return soundcloud;
    }

    public void setSoundcloud(SoundCloud soundcloud) {
        this.soundcloud = soundcloud;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getScclientid() {
        return scclientid;
    }

    public void setScclientid(String scclientid) {
        this.scclientid = scclientid;
    }

    public String getScclientsecret() {
        return scclientsecret;
    }

    public void setScclientsecret(String scclientsecret) {
        this.scclientsecret = scclientsecret;
    }
}